// Constant categories
export const CATEGORIES = [
  { id: 1, name: "Kinh Doanh" },
  { id: 2, name: "Thời Sự" },
  { id: 3, name: "Thể Thao" },
  { id: 4, name: "Giải Trí" },
  { id: 5, name: "Giải Trí" },
  { id: 6, name: "Giải Trí" },
  { id: 7, name: "Giải Trí" },
  { id: 8, name: "Giải Trí" },
  { id: 9, name: "Giải Trí" },
  { id: 10, name: "Giải Trí" },
  { id: 11, name: "Giải Trí" },
  { id: 12, name: "Giải Trí" },
  { id: 13, name: "Giải Trí" },
];

// Constant positions
export const POSITIONS = [
  { id: 1, name: "Việt Nam" },
  { id: 2, name: "Châu Á" },
  { id: 3, name: "Châu Âu" },
  { id: 4, name: "Châu Mỹ" },
];

export const IPAPI = "http://localhost:3000/blogs/";
